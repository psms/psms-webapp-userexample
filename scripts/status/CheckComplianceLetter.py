import lib.diststatus as diststatus

class CheckComplianceLetter(diststatus.Status):
    '''
        # Letter of compliance check status

        to be documented
    '''
    __name__    = 'Letter of compliance'
    __author__  = 'Julian Maxime Mendez'
    __version__ = 'v.1.0'

    def call(self, inputs):
        '''
            To be documented
        '''
        if inputs['Skip checking'] == False:
            for p in self.order_details['parts']:
                remains = p['quantity']-p['qty_assigned']
                if remains > 0:
                    raise Exception('At least one part is not assigned to a batch')

        return self.status_id+1

    def preset(self):
        '''
            To be documented
        '''

        budget_code = self.order_details['details']['Budget code']

        return [
            {
                'type': 'boolean',
                'name': 'Skip checking',
                'desc': 'Warning: skipping the check might cause some troubles into the system. Are you sure that you want to skip it?',
                'default': False,
                'options': []
            }]

    def display(self):
        '''
            To be documented
        '''
        return {}

    def check(self):
        for p in self.order_details['parts']:
            remains = p['quantity']-p['qty_assigned']
            if remains > 0:
                return False

        return True