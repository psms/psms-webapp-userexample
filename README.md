# User configuration for Distribution Management System

## Configuration of order handling

The scripts to be used will go on the `scripts` folder. 

The user scripts for each different purchase status need to be in the folder `scripts/status/`.

## Configuration of custom pages in the client website
The client website has the following structure:

- */distribution/order* - Order Page : lists all available parts for purchase.
- */distribution/myorders* - My Orders Page : lists all orders made by the user.
- */account* - User Account Page : lists the user details.
- Custom pages : in addition to the default routes, the user can define any number of custom pages.

The custom pages are configured and defined in the `pages/` directory.


### Configuration file - `pages/routes.config`
Custom pages are configured in the  `pages/routes.config` file. The are defined by an `.html` file or an `.md` file in the `/pages` directory.

The parameters that can be defined for a custom page are:

- **Title** \<pageid\>_title : The name that we want to give the page. It is the name that will appear in the navigation bar directing to the page. For example: `"Chip 1 Manual"`.
- **URL** \<pageid\>_url: The path that we want the page to have inside the navigation of the page. For example: `"/chip1manual"`. 

*Note* - A page with a compound url will be treated as part of a section if a section header with the first part of its url has been defined. For example : a page with url `'/documentation/chip1manual'` will be treated as part of the section `/documentation` if this has been defined.


- **Filename** \<pageid\>_filename : The filename of the  `.html` or `.md` file that has the contents of the page. (Optional for section headers)

*Note* - A section header without a filename defined will direct when clicked to a page with a list of hyperlinks to the pages contained in the section.


- **Section** (optional) \<pageid\>_issection : Defines if the route is a section header or not. Section headers will appear as such in the navigation bar. The pages inside the section will be those with a compound url.
- **Icon** (optional) \<pageid\>_icon : Defines an icon for the page to show on the navigation bar. The content of this parameter has to be a [Font-Awesome](https://fontawesome.com/v5.15/icons?d=gallery&p=2) icon name (format : *fa-\<icon name\>*) .


A route is defined in the following way:
```
#pages/routes.config

#Page name
<pageid>_title='<Desired title>'
<page name>_url='<Desired URL>'
<page name>_filename='<Filename of the content of the page>'
<pageid>_issection=yes
<pageid>_icon=<Fa code for the desired icon>
```

A list of all the pages has to be provided in the `routes` parameter:
```
#pages/routes.config

#All routes
#The name of all the routes in the user defined navigation.
routes='<pageid 1> <pageid 2> <...> <pageid n>'
```



#### **Simple example**
In this example, two pages will be defined:

- 'Page 1', with path '/route1' and associated file 'page1.md'. No icon is defined.
- 'Page 2', with path '/route2' and associated file 'page2.html'. The icon 'fa-bath' is defined.

```
#pages/routes.config

#All routes
#The name of all the routes in the user defined navigation.
routes='page1 page2'

#Page 1 
page1_title='Page 1'
page1_url='/route1'
page1_filename='page1.md'

#Page 2 
page2_title='Page 2'
page2_url='/route2'
page2_filename='page2.html'
```
Contents of `/pages`:

- `routes.config`
- `page1.md`
- `page2.md`




#### **Example with sections**
In this example, 4 pages will be defined, with 2 being part of a section:

- 'Route 1', with path '/projectoverview/route1' and associated file 'route1.md'. This page is part of the 'Project Overview' section. The icon 'fa-bath' is defined.
- 'Route 2', with path '/projectoverview/route2' and associated file 'route2.html'. This page is part of the 'Project Overview' section. No icon is defined.
- 'Route 3', with path '/route3' and associated file 'route3_final.html'. No icon.
- 'Route 4', with path '/route4' and associated file 'route4.md'. No icon.

<br>

- The section, 'Project Overview', has path '/projectoverview' and no associated file.


```
#All routes
#The name of all the routes in the user defined navigation.
routes='routesection route1 route2 route3 routeimportant'

#Section header
routesection_title='Project Overview'
routesection_url='/projectoverview'
routesection_issection=yes

#Route 1 
route1_title='Route 1'
route1_url='/projectoverview/route1'
route1_filename='route1.md'
route1_icon=fa-bath

#Route 2 
route2_title='Route 2'
route2_url='/projectoverview/route2'
route2_filename='route2.html'

#Route 3 
route3_title='Route 3'
route3_url='/route3'
route3_filename='route3_final.html'

#Route Important 
routeimportant_title='Route 4'
routeimportant_url='/route4'
routeimportant_filename='route4.md'
```

The contents of `/pages` are: 

- `routes.config`
- `route1.md`
- `route2.html`
- `route3_final.html`
- `route4.md`

#### **Creating a home page**
To create a home page we need to set the url to '/' and the section flag.
```
# pages/routes.config

#Home
home_title='Home'
home_url='/'
home_filename='home.html'
home_issection=yes
```

The `'home'` page should be the **first** one in the list of `routes`:
```
#pages/routes.config

#All routes
#The name of all the routes in the user defined navigation.
routes='home <etc>'
```
